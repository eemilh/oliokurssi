package com.example.myapplication2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;


public class MainActivity extends AppCompatActivity {


    TextView text;
    TextInputEditText textinput, summainput, summainput2, summainput3, summainput4;
    Bank bank;
    String apu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.testFunction();
        // Creating references to activity_main.xml components
        text = (TextView) findViewById(R.id.textView);
        textinput=(TextInputEditText) findViewById(R.id.nimi_input);
        summainput=(TextInputEditText) findViewById(R.id.summa_input);
        summainput2=(TextInputEditText) findViewById(R.id.summa_input2);
        summainput3=(TextInputEditText) findViewById(R.id.summa_input3);
        summainput4=(TextInputEditText) findViewById(R.id.summa_input4);
        bank=new Bank();
        //bank.addKäyttötili();
        //bank.tulostaTilit();
        ColorStateList oldColors =  text.getTextColors();


    }

    public void testFunction (View v) {

        //System.out.println("HELLO WORLD!");
        text.setText("Hello");
        //bank.tulostaTilit();
        text.setText(textinput.getEditableText());
    }

    public void perusta_tili(View v){
        //apu=textinput.getEditableText().toString();
        //bank.addKäyttötili();
        if ((textinput.getEditableText().toString()).isEmpty()) {
            text.setText("Anna nimesi ensin.");
            //text.setHighlightColor(Color.RED);
            //text.setHighlightColor(Color.);

            return;
        }
        // Create a user account if the user does not enter a credit line. Otherwise, a credit account will be created
        if (summainput4.getEditableText().toString().isEmpty()) {
            bank.addKäyttötili(textinput.getEditableText().toString());
        }
        else {
                bank.addLuottotili(textinput.getEditableText().toString(),Double.parseDouble(summainput4.getEditableText().toString()));
            }
        //Bank bank=new Bank();
        apu=bank.tulostaTilitAsText();
        text.setText(apu);
    }

    public void teetilitapahtuma(View v){
        //apu=textinput.getEditableText().toString();
        //bank.addKäyttötili();

        if (((summainput.getEditableText().toString()).isEmpty()) || ((summainput2.getEditableText().toString()).isEmpty())) {
            text.setText("Anna ensin summa ja tilinumero");
            return;
        }
        // If no destination account number is entered, a standard deposit / withdrawal / payment will be made
        if ((summainput3.getEditableText().toString()).isEmpty()){
            bank.talleta(bank.etsiTili(Integer.parseInt(summainput2.getEditableText().toString())), Double.parseDouble(summainput.getEditableText().toString()));
        }
        // otherwise a bank transfer will be made between the bank accounts
        else {
            bank.talleta(bank.etsiTili(Integer.parseInt(summainput2.getEditableText().toString())), -1 * Double.parseDouble(summainput.getEditableText().toString()));
            bank.talleta(bank.etsiTili(Integer.parseInt(summainput3.getEditableText().toString())), Double.parseDouble(summainput.getEditableText().toString()));
            //text.setText("moi");
        }
        apu=bank.tulostaTilitAsText(); // print an updated list of bank accounts
        text.setText(apu);
        // clear the input fields after completing the account transaction
        summainput.setText("");
        summainput2.setText("");
        summainput3.setText("");
    }

    public void tulostatilintapahtumat(View v){
        if ((summainput2.getEditableText().toString()).isEmpty()){
            text.setText("Et antanut tilinumeroa. Anna tilinumero.");
        }
        else {
            apu = bank.tulostaTilitapahtumattAsText(Integer.parseInt(summainput2.getEditableText().toString()));
            text.setText(apu);
        }
    }

}
